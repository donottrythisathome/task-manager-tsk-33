package com.ushakov.tm.api;

import com.ushakov.tm.model.AbstractEntity;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    @NotNull
    E add(@NotNull E entity);

    void addAll(@Nullable List<E> list);

    void clear();

    @Nullable
    List<E> findAll();

    @Nullable
    E findOneById(@NotNull String id);

    void remove(@NotNull E entity);

    @Nullable
    E removeOneById(@NotNull String id);

}
