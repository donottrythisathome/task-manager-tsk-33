package com.ushakov.tm.repository;

import com.ushakov.tm.api.repository.IProjectRepository;
import com.ushakov.tm.model.Project;

public class ProjectRepository extends AbstractOwnerBusinessRepository<Project> implements IProjectRepository {
}