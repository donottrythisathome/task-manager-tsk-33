package com.ushakov.tm.command.auth;

import com.ushakov.tm.command.AbstractUserCommand;
import com.ushakov.tm.endpoint.Session;
import com.ushakov.tm.endpoint.User;
import com.ushakov.tm.exception.entity.UserNotFoundException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public class UserViewProfileCommand extends AbstractUserCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @Nullable
    public String description() {
        return "View Current User Profile.";
    }

    @Override
    public void execute() {
        @NotNull final Session session = endpointLocator.getSession();
        @Nullable final User user = endpointLocator.getAdminUserEndpoint().findUserById(session, session.getUserId());
        Optional.ofNullable(user).orElseThrow(UserNotFoundException::new);
        System.out.println("[VIEW PROFILE]:");
        showUser(user);
    }

    @Override
    @NotNull
    public String name() {
        return "user-view-profile";
    }

}
