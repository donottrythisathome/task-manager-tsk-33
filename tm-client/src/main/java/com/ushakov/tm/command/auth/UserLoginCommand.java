package com.ushakov.tm.command.auth;

import com.ushakov.tm.command.AbstractUserCommand;
import com.ushakov.tm.endpoint.Session;
import com.ushakov.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class UserLoginCommand extends AbstractUserCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @Nullable
    public String description() {
        return "New login.";
    }

    @Override
    public void execute() {
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        @NotNull final String password = TerminalUtil.nextLine();
        @NotNull final Session session = endpointLocator.getSessionEndpoint().openSession(login, password);
        endpointLocator.setSession(session);
        System.out.println("SUCCESS");
    }

    @Override
    @NotNull
    public String name() {
        return "login";
    }

}
