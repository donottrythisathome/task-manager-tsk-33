package com.ushakov.tm.endpoint;

import com.ushakov.tm.api.endpoint.ISessionEndpoint;
import com.ushakov.tm.api.service.ISessionService;
import com.ushakov.tm.model.Session;
import org.jetbrains.annotations.NotNull;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public class SessionEndpoint extends AbstractEndpoint implements ISessionEndpoint {

    private final ISessionService sessionService;

    public SessionEndpoint(final ISessionService sessionService) {
        this.sessionService = sessionService;
    }

    @WebMethod
    public void closeSession(@NotNull @WebParam(name = "session") final Session session) {
        sessionService.close(session);
    }

    @WebMethod
    public @NotNull Session openSession(
            @NotNull @WebParam(name = "login") final String userLogin,
            @NotNull @WebParam(name = "password") final String password
    ) {
        return sessionService.open(userLogin, password);
    }


}
