package com.ushakov.tm.command.task;

import com.ushakov.tm.command.AbstractTaskCommand;
import com.ushakov.tm.endpoint.Session;
import com.ushakov.tm.endpoint.Task;
import com.ushakov.tm.enumerated.Role;
import com.ushakov.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class TaskBindByProjectIdCommand extends AbstractTaskCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @Nullable
    public String description() {
        return "Bind task to project.";
    }

    @Override
    public void execute() {
        @NotNull final Session session = endpointLocator.getSession();
        System.out.println("ENTER PROJECT ID");
        @NotNull final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID");
        @NotNull final String taskId = TerminalUtil.nextLine();
        @NotNull final Task task = endpointLocator.getProjectEndpoint().bindTaskToProject(session, projectId, taskId);
    }

    @Override
    @NotNull
    public String name() {
        return "bind-task-by-project-id";
    }

    @Override
    @Nullable
    public Role[] roles() {
        return Role.values();
    }

}
