package com.ushakov.tm.command.user;

import com.ushakov.tm.command.AbstractUserCommand;
import com.ushakov.tm.endpoint.Session;
import com.ushakov.tm.endpoint.User;
import com.ushakov.tm.enumerated.Role;
import com.ushakov.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class UserRemoveByLoginCommand extends AbstractUserCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @Nullable
    public String description() {
        return "Remove user by login.";
    }

    @Override
    public void execute() {
        System.out.println("ENTER USER LOGIN");
        @NotNull final Session session = endpointLocator.getSession();
        @NotNull final String login = TerminalUtil.nextLine();
        @NotNull final User user = endpointLocator.getAdminUserEndpoint().removeUserByLogin(session, login);
    }

    @Override
    @NotNull
    public String name() {
        return "user-remove-by-login";
    }

    @Override
    @Nullable
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
