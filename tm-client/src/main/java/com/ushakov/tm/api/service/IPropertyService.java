package com.ushakov.tm.api.service;

import com.ushakov.tm.api.other.ISaltSettings;
import org.jetbrains.annotations.NotNull;

public interface IPropertyService extends ISaltSettings {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getDeveloperEmail();

    @NotNull
    String getDeveloperName();

}