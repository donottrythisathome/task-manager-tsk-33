package com.ushakov.tm.command.task;

import com.ushakov.tm.command.AbstractTaskCommand;
import com.ushakov.tm.endpoint.Session;
import com.ushakov.tm.endpoint.Task;
import com.ushakov.tm.enumerated.Role;
import com.ushakov.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class TaskCompleteByIndexCommand extends AbstractTaskCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @Nullable
    public String description() {
        return "Complete task by index.";
    }

    @Override
    public void execute() {
        @NotNull final Session session = endpointLocator.getSession();
        System.out.println("ENTER TASK INDEX");
        @NotNull final Integer taskIndex = TerminalUtil.nextNumber();
        @NotNull final Task task = endpointLocator.getTaskEndpoint().completeTaskByIndex(session, taskIndex - 1);
    }

    @Override
    @NotNull
    public String name() {
        return "complete-task-by-index";
    }

    @Override
    @Nullable
    public Role[] roles() {
        return Role.values();
    }

}
