package com.ushakov.tm.command.system;

import com.jcabi.manifests.Manifests;
import com.ushakov.tm.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class AboutCommand extends AbstractCommand {
    @Override
    @Nullable
    public String arg() {
        return "-a";
    }

    @Override
    @Nullable
    public String description() {
        return "Show developer info.";
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("NAME: ");
        System.out.println(Manifests.read("developer"));
        System.out.println("E-MAIL: ");
        System.out.println(Manifests.read("email"));
    }

    @Override
    @NotNull
    public String name() {
        return "about";
    }

}
