package com.ushakov.tm.command.task;

import com.ushakov.tm.command.AbstractTaskCommand;
import com.ushakov.tm.endpoint.Session;
import com.ushakov.tm.endpoint.Task;
import com.ushakov.tm.enumerated.Role;
import com.ushakov.tm.enumerated.Sort;
import com.ushakov.tm.exception.entity.ObjectNotFoundException;
import com.ushakov.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class TaskShowListCommand extends AbstractTaskCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @Nullable
    public String description() {
        return "Show task list.";
    }

    @Override
    public void execute() {
        @NotNull final Session session = endpointLocator.getSession();
        System.out.println("[TASK LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        @NotNull final String sort = TerminalUtil.nextLine();
        @Nullable List<Task> tasks = endpointLocator.getTaskEndpoint().findAllTasks(session);
        int index = 1;
        Optional.ofNullable(tasks).orElseThrow(ObjectNotFoundException::new);
        for (@NotNull final Task task : tasks) {
            System.out.println(index + ". " + task);
            index++;
        }
    }

    @Override
    @NotNull
    public String name() {
        return "task-list";
    }

    @Override
    @Nullable
    public Role[] roles() {
        return Role.values();
    }

}
