package com.ushakov.tm.endpoint;

import com.ushakov.tm.api.endpoint.IProjectEndpoint;
import com.ushakov.tm.api.service.IProjectService;
import com.ushakov.tm.api.service.IProjectTaskService;
import com.ushakov.tm.api.service.ISessionService;
import com.ushakov.tm.enumerated.Status;
import com.ushakov.tm.model.Project;
import com.ushakov.tm.model.Session;
import com.ushakov.tm.model.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    private IProjectService projectService;

    private ISessionService sessionService;

    private IProjectTaskService projectTaskService;

    public ProjectEndpoint() {

    }

    public ProjectEndpoint(final IProjectService projectService, ISessionService sessionService, IProjectTaskService projectTaskService) {
        this.projectService = projectService;
        this.sessionService = sessionService;
        this.projectTaskService = projectTaskService;
    }

    @Override
    @WebMethod
    public void addAllProjects(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "entities") List<Project> entities
    ) {
        sessionService.validate(session);
        projectService.addAll(session.getUserId(), entities);
    }

    @Override
    @WebMethod
    public void addProject(@Nullable @WebParam(name = "session") Session session,
                           @Nullable @WebParam(name = "entity") Project entity
    ) {
        sessionService.validate(session);
        projectService.add(session.getUserId(), entity);
    }

    @Override
    @WebMethod
    public @Nullable Task bindTaskToProject(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "taskId") final String taskId,
            @Nullable @WebParam(name = "projectId") final String projectId
    ) {
        sessionService.validate(session);
        return projectTaskService.bindTaskByProjectId(session.getUserId(), taskId, projectId);
    }


    @Override
    @WebMethod
    public @NotNull Project changeProjectStatusById(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "id") String id,
            @Nullable @WebParam(name = "status") Status status
    ) {
        sessionService.validate(session);
        return projectService.changeStatusById(session.getUserId(), id, status);
    }

    @Override
    @WebMethod
    public @NotNull Project changeProjectStatusByIndex(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "index") Integer index,
            @Nullable @WebParam(name = "status") Status status
    ) {
        sessionService.validate(session);
        return projectService.changeStatusByIndex(session.getUserId(), index, status);
    }

    @Override
    @WebMethod
    public @NotNull Project changeProjectStatusByName(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "name") String name,
            @Nullable @WebParam(name = "status") Status status
    ) {
        sessionService.validate(session);
        return projectService.changeStatusByName(session.getUserId(), name, status);
    }

    @Override
    @WebMethod
    public void clearProjects(@Nullable @WebParam(name = "session") Session session) {
        sessionService.validate(session);
        projectService.clear(session.getUserId());
    }

    @Override
    @WebMethod
    public @NotNull Project completeProjectById(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "id") String id
    ) {
        sessionService.validate(session);
        return projectService.completeById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public @NotNull Project completeProjectByIndex(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "index") Integer index
    ) {
        sessionService.validate(session);
        return projectService.completeByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    public @NotNull Project completeProjectByName(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "name") String name
    ) {
        sessionService.validate(session);
        return projectService.completeByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public void createProject(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "name") String name,
            @Nullable @WebParam(name = "description") String description
    ) {
        sessionService.validate(session);
        projectService.add(session.getUserId(), name, description);
    }

    @Override
    @WebMethod
    @Nullable
    public List<Project> findAllProjects(@Nullable @WebParam(name = "session") Session session) {
        sessionService.validate(session);
        return projectService.findAll(session.getUserId());
    }

    @Override
    @WebMethod
    public @NotNull Project findProjectById(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "id") String id
    ) {
        sessionService.validate(session);
        return projectService.findOneById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public @NotNull Project findProjectByIndex(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "index") Integer index
    ) {
        sessionService.validate(session);
        return projectService.findOneByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    public @NotNull Project findProjectByName(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "name") String name
    ) {
        sessionService.validate(session);
        return projectService.findOneByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public @Nullable Project removeProjectById(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "id") String id
    ) {
        sessionService.validate(session);
        return projectService.removeOneById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public @Nullable Project removeProjectByIndex(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "index") Integer index
    ) {
        sessionService.validate(session);
        return projectService.removeOneByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    public @Nullable Project removeProjectByName(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "name") String name
    ) {
        sessionService.validate(session);
        return projectService.removeOneByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public @NotNull Project startProjectById(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "id") String id
    ) {
        sessionService.validate(session);
        return projectService.startById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public @NotNull Project startProjectByIndex(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "index") Integer index
    ) {
        sessionService.validate(session);
        return projectService.startByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    public @NotNull Project startProjectByName(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "name") String name
    ) {
        sessionService.validate(session);
        return projectService.startByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public void updateProjectById(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "id") String id,
            @Nullable @WebParam(name = "name") String name,
            @NotNull @WebParam(name = "description") String description
    ) {
        sessionService.validate(session);
        projectService.updateById(session.getUserId(), id, name, description);
    }

    @Override
    @WebMethod
    public void updateProjectByIndex(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "index") Integer index,
            @Nullable @WebParam(name = "name") String name,
            @NotNull @WebParam(name = "description") String description
    ) {
        sessionService.validate(session);
        projectService.updateByIndex(session.getUserId(), index, name, description);
    }

}
