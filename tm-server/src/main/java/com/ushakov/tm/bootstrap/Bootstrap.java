package com.ushakov.tm.bootstrap;

import com.ushakov.tm.api.repository.IProjectRepository;
import com.ushakov.tm.api.repository.ISessionRepository;
import com.ushakov.tm.api.repository.ITaskRepository;
import com.ushakov.tm.api.repository.IUserRepository;
import com.ushakov.tm.api.service.*;
import com.ushakov.tm.component.Backup;
import com.ushakov.tm.endpoint.*;
import com.ushakov.tm.enumerated.Role;
import com.ushakov.tm.repository.ProjectRepository;
import com.ushakov.tm.repository.SessionRepository;
import com.ushakov.tm.repository.TaskRepository;
import com.ushakov.tm.repository.UserRepository;
import com.ushakov.tm.service.*;
import com.ushakov.tm.util.SystemUtil;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Bootstrap implements ServiceLocator {

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository();

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(taskRepository, projectRepository);

    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final IUserService userService = new UserService(userRepository, propertyService);

    @NotNull
    private final IAuthService authService = new AuthService(userService, propertyService);

    @NotNull
    private final IDataService dataService = new DataService(projectService, taskService, userService);

    @NotNull
    private final ISessionService sessionService = new SessionService(userService, sessionRepository, propertyService);

    @NotNull
    private final ProjectEndpoint projectEndpoint = new ProjectEndpoint(projectService, sessionService, projectTaskService);

    @NotNull
    private final TaskEndpoint taskEndpoint = new TaskEndpoint(taskService, sessionService);

    @NotNull
    private final UserEndpoint userEndpoint = new UserEndpoint(sessionService, userService);

    @NotNull
    private final AdminUserEndpoint adminUserEndpoint = new AdminUserEndpoint();

    @NotNull
    private final SessionEndpoint sessionEndpoint = new SessionEndpoint(sessionService);

    @NotNull
    public Backup backup = new Backup(this, dataService);

    @NotNull
    private final AdminDataEndpoint adminDataEndpoint = new AdminDataEndpoint(sessionService, backup, dataService);

    @Override
    @NotNull
    public IAuthService getAuthService() {
        return authService;
    }

    @Override
    @NotNull
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    @NotNull
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @Override
    @NotNull
    public IPropertyService getPropertyService() {
        return propertyService;
    }

    @Override
    @NotNull
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    @NotNull
    public IUserService getUserService() {
        return userService;
    }

    private void initData() {
        projectService.add(userService.findOneByLogin("user").getId(), "Project1", "First Project");
        projectService.add(userService.findOneByLogin("admin").getId(), "Project2", "Second Project");
        taskService.add(userService.findOneByLogin("user").getId(), "MyFirstTask", "Sample");
        taskService.add(userService.findOneByLogin("admin").getId(), "AnotherTask", "Another One");
    }

    private void initEndpoint() {
        registryEndpoint(projectEndpoint);
        registryEndpoint(taskEndpoint);
        registryEndpoint(sessionEndpoint);
        registryEndpoint(adminDataEndpoint);
        registryEndpoint(adminUserEndpoint);
        registryEndpoint(userEndpoint);
    }

    @SneakyThrows
    public void initPID() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    private void initUsers() {
        userService.add("user", "user", "try@mail.ru");
        userService.add("admin", "admin", Role.ADMIN);
    }

    private void registryEndpoint(final @NotNull Object endpoint) {
        final String host = propertyService.getServerHost();
        final String port = propertyService.getServerPort();
        final String name = endpoint.getClass().getSimpleName();
        final String wsdl = "http://" + host + ":" + port + "/" + name + "?wsdl";
        System.out.println(wsdl);
        Endpoint.publish(wsdl, endpoint);
    }

    public void run(@Nullable final String... args) {
        initUsers();
        initEndpoint();
        initData();
        backup.init();
    }

}
