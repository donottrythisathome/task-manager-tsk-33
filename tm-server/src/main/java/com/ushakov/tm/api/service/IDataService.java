package com.ushakov.tm.api.service;

import lombok.SneakyThrows;

public interface IDataService {

    @SneakyThrows
    void loadBackup();

    @SneakyThrows
    void loadDataBase64();

    @SneakyThrows
    void loadDataBin();

    @SneakyThrows
    void loadDataJson();

    @SneakyThrows
    void loadDataJsonJaxB();

    @SneakyThrows
    void loadDataXml();

    @SneakyThrows
    void loadDataXmlJaxB();

    @SneakyThrows
    void loadDataYaml();

    @SneakyThrows
    void saveBackup();

    @SneakyThrows
    void saveDataBase64();

    @SneakyThrows
    void saveDataBin();

    @SneakyThrows
    void saveDataJson();

    @SneakyThrows
    void saveDataJsonJaxB();

    @SneakyThrows
    void saveDataXml();

    @SneakyThrows
    void saveDataXmlJaxB();

    @SneakyThrows
    void saveDataYaml();

}
