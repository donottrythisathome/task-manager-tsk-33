package com.ushakov.tm.service;

import com.ushakov.tm.api.repository.IUserRepository;
import com.ushakov.tm.api.service.IPropertyService;
import com.ushakov.tm.api.service.IUserService;
import com.ushakov.tm.enumerated.Role;
import com.ushakov.tm.exception.empty.*;
import com.ushakov.tm.exception.entity.UserAlreadyExistsException;
import com.ushakov.tm.exception.entity.UserNotFoundException;
import com.ushakov.tm.model.User;
import com.ushakov.tm.util.HashUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    private final IUserRepository userRepository;

    @NotNull
    private final IPropertyService propertyService;

    public UserService(
            @NotNull final IUserRepository userRepository,
            @NotNull final IPropertyService propertyService
    ) {
        super(userRepository);
        this.userRepository = userRepository;
        this.propertyService = propertyService;
    }

    @Override
    @NotNull
    public User add(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (userRepository.findOneByLogin(login) != null) throw new UserAlreadyExistsException("login");
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(Role.USER);
        return userRepository.add(user);
    }

    @Override
    @NotNull
    public User add(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        if (userRepository.findOneByEmail(email) != null) throw new UserAlreadyExistsException("email");
        @NotNull final User user = add(login, password);
        user.setEmail(email);
        return user;
    }

    @Override
    @NotNull
    public User add(@Nullable final String login, @Nullable final String password, @Nullable final Role role) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();
        @NotNull final User user = add(login, password);
        user.setRole(role);
        return user;
    }

    @Override
    @NotNull
    public User findOneByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable User user = userRepository.findOneByLogin(login);
        Optional.ofNullable(user).orElseThrow(UserNotFoundException::new);
        return user;
    }

    @Override
    @NotNull
    public User lockByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @NotNull final User user = findOneByLogin(login);
        user.setLocked(true);
        return user;
    }

    @Override
    @NotNull
    public User removeOneByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable User user = userRepository.removeOneByLogin(login);
        Optional.ofNullable(user).orElseThrow(UserNotFoundException::new);
        return user;
    }

    @Override
    @NotNull
    public User setPassword(@Nullable final String userId, @Nullable final String password) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @Nullable final User user = findOneById(userId);
        Optional.ofNullable(user).orElseThrow(UserNotFoundException::new);
        @NotNull final String hash = HashUtil.salt(propertyService, password);
        return user;
    }

    @Override
    @NotNull
    public User unlockByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @NotNull final User user = findOneByLogin(login);
        user.setLocked(false);
        return user;
    }

    @Override
    @NotNull
    public User updateUser(
            @Nullable final String userId,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        @Nullable final User user = findOneById(userId);
        Optional.ofNullable(user).orElseThrow(UserNotFoundException::new);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        return user;
    }

}
