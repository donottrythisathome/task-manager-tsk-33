package com.ushakov.tm.api.service;

import com.ushakov.tm.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface ICommandService {

    void add(@NotNull AbstractCommand command);

    @Nullable
    AbstractCommand getCommandByArg(@Nullable String arg);

    @Nullable
    AbstractCommand getCommandByName(@Nullable String name);

    @NotNull
    List<String> getCommandNames();

    @NotNull
    List<AbstractCommand> getCommands();

    @NotNull
    List<AbstractCommand> getCommandsWithArguments();

}
