package com.ushakov.tm.api.repository;

import com.ushakov.tm.api.IRepository;
import com.ushakov.tm.model.Session;
import org.jetbrains.annotations.NotNull;

public interface ISessionRepository extends IRepository<Session> {

    boolean contains(@NotNull String id);

}
