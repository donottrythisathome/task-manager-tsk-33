package com.ushakov.tm.api.endpoint;

import com.ushakov.tm.enumerated.Role;
import com.ushakov.tm.model.Session;
import com.ushakov.tm.model.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface IAdminUserEndpoint {

    @WebMethod
    void addAllUsers(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "entities") List<User> entities
    );

    @WebMethod
    void addUser(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "entity") User entity
    );

    @WebMethod
    void clearUsers(@Nullable @WebParam(name = "session") Session session);

    @WebMethod
    @NotNull User createUser(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "login") String userLogin,
            @Nullable @WebParam(name = "password") String password
    );

    @WebMethod
    @NotNull User createWithEmail(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "login") String userLogin,
            @Nullable @WebParam(name = "password") String password,
            @Nullable @WebParam(name = "email") String email
    );

    @WebMethod
    @NotNull User createWithRole(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "login") String userLogin,
            @Nullable @WebParam(name = "password") String password,
            @Nullable @WebParam(name = "role") Role role
    );

    @WebMethod
    @NotNull List<User> findAllUsers(@Nullable @WebParam(name = "session") Session session);

    @WebMethod
    @Nullable User findUserById(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "id") String id
    );

    @WebMethod
    @Nullable User findUserByLogin(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "login") String userLogin
    );

    @WebMethod
    @NotNull User lockUserByLogin(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "login") final String login
    );

    @WebMethod
    void removeUser(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "entity") User entity
    );

    @WebMethod
    @Nullable User removeUserById(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "id") String id
    );

    @WebMethod
    public @NotNull User removeUserByLogin(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "login") String userLogin
    );

    @WebMethod
    void setUserPassword(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "id") String id,
            @Nullable @WebParam(name = "password") String password
    );

    @WebMethod
    @NotNull User unlockUserByLogin(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "login") final String login
    );

    @WebMethod
    void updateUserById(
            @WebParam(name = "session", partName = "session") final Session session,
            @WebParam(name = "firstName", partName = "firstName") final String firstName,
            @WebParam(name = "lastName", partName = "lastName") final String lastName,
            @WebParam(name = "middleName", partName = "middleName") final String middleName
    );

}
