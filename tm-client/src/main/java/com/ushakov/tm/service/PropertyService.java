package com.ushakov.tm.service;

import com.jcabi.manifests.Manifests;
import com.ushakov.tm.api.service.IPropertyService;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.InputStream;
import java.util.Properties;

public class PropertyService implements IPropertyService {

    @NotNull
    private static final String FILE_NAME = "application.properties";

    @NotNull
    private static final String APPLICATION_VERSION_DEFAULT = "";

    @NotNull
    private static final String APPLICATION_VERSION_KEY = "application.version";

    @NotNull
    private static final String PASSWORD_ITERATION_DEFAULT = "1";

    @NotNull
    private static final String PASSWORD_ITERATION_KEY = "password.iteration";

    @NotNull
    private static final String PASSWORD_SECRET_DEFAULT = "";

    @NotNull
    private static final String PASSWORD_SECRET_KEY = "password.secret";

    @NotNull
    private static final String APPLICATION_DEVELOPER_KEY = "developer.name";

    @NotNull
    private static final String APPLICATION_DEVELOPER_DEFAULT = "Ivan Ushakov";

    @NotNull
    private static final String DEVELOPER_EMAIL_KEY = "developer.email";

    @NotNull
    private static final String DEVELOPER_EMAIL_DEFAULT = "iushakov@t1-consulting.com";


    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        @Nullable final InputStream inputStream = ClassLoader.getSystemResourceAsStream(FILE_NAME);
        if (inputStream == null) return;
        properties.load(inputStream);
        inputStream.close();
    }

    @NotNull
    @Override
    public String getApplicationVersion() {
        return getManifestValue("build", APPLICATION_VERSION_DEFAULT);
    }

    @NotNull
    @Override
    public String getDeveloperEmail() {
        return getManifestValue("email", DEVELOPER_EMAIL_DEFAULT);
    }

    @NotNull
    @Override
    public String getDeveloperName() {
        return getManifestValue("developer", APPLICATION_DEVELOPER_DEFAULT);
    }

    @NotNull
    private String getManifestValue(@NotNull final String manifest, @NotNull final String defaultValue) {
        if (Manifests.exists(manifest)) return Manifests.read(manifest);
        return defaultValue;
    }

    @NotNull
    @Override
    public Integer getPasswordIteration() {
        @Nullable final String systemProperty = System.getProperty(PASSWORD_ITERATION_KEY);
        if (systemProperty != null) return Integer.parseInt(systemProperty);
        @Nullable final String enviromentProperty = System.getenv(PASSWORD_ITERATION_KEY);
        if (enviromentProperty != null) return Integer.parseInt(enviromentProperty);
        return Integer.parseInt(properties.getProperty(PASSWORD_ITERATION_KEY, PASSWORD_ITERATION_DEFAULT));
    }

    @NotNull
    @Override
    public String getPasswordSecret() {
        return getValue(PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT);
    }

    @NotNull
    private String getValue(@NotNull final String name, @NotNull final String defaultValue) {
        if (System.getProperties().containsKey(name)) return System.getProperty(name);
        if (System.getenv().containsKey(name)) return System.getProperty(name);
        return properties.getProperty(name, defaultValue);
    }

}
