package com.ushakov.tm.command.data.json;

import com.ushakov.tm.command.AbstractDataCommand;
import com.ushakov.tm.endpoint.Session;
import com.ushakov.tm.enumerated.Role;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class DataJsonLoadJaxBCommand extends AbstractDataCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @Nullable
    public String description() {
        return "Load data from json by JAXB.";
    }

    @SneakyThrows
    @Override
    public void execute() {
        @NotNull final Session session = endpointLocator.getSession();
        endpointLocator.getAdminDataEndpoint().loadDataJsonJaxB(session);
    }

    @Override
    @NotNull
    public String name() {
        return "data-load-json-jaxb";
    }

    @Override
    @Nullable
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
