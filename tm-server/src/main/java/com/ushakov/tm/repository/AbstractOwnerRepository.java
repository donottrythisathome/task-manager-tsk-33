package com.ushakov.tm.repository;

import com.ushakov.tm.api.repository.IOwnerRepository;
import com.ushakov.tm.exception.entity.ObjectNotFoundException;
import com.ushakov.tm.model.AbstractOwnerEntity;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public abstract class AbstractOwnerRepository<E extends AbstractOwnerEntity>
        extends AbstractRepository<E>
        implements IOwnerRepository<E> {

    @Override
    @NotNull
    public E add(@NotNull final String userId, @NotNull final E entity) {
        entity.setUserId(userId);
        list.add(entity);
        return entity;
    }

    @Override
    public void clear(@NotNull final String userId) {
        list.removeAll(list.stream()
                .filter(e -> userId.equals(e.getUserId()))
                .collect(Collectors.toList()));
    }

    @Override
    @Nullable
    public List<E> findAll(@NotNull final String userId) {
        return list.stream()
                .filter(e -> userId.equals(e.getUserId()))
                .collect(Collectors.toList());
    }

    @Override
    @Nullable
    public List<E> findAll(@NotNull final String userId, @NotNull Comparator<E> comparator) {
        return list.stream()
                .filter(e -> userId.equals(e.getUserId()))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Override
    @Nullable
    public E findOneById(@NotNull final String userId, @NotNull final String id) {
        @Nullable final List<E> list = findAll(userId);
        Optional.ofNullable(list).orElseThrow(ObjectNotFoundException::new);
        return list.stream()
                .filter(e -> e.getId().equals(id))
                .findFirst().orElse(null);
    }

    @Override
    @Nullable
    public E findOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @Nullable final E entity = list.get(index);
        Optional.ofNullable(entity).orElseThrow(ObjectNotFoundException::new);
        if (userId.equals(entity.getUserId())) return entity;
        return null;
    }

    @Override
    @Nullable
    public E removeOneById(@NotNull final String userId, @NotNull final String id) {
        @Nullable final E entity = findOneById(userId, id);
        Optional.ofNullable(entity).ifPresent(list::remove);
        return entity;
    }

    @Override
    @Nullable
    public E removeOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @Nullable final E entity = findOneByIndex(userId, index);
        Optional.ofNullable(entity).ifPresent(list::remove);
        return entity;
    }

}