package com.ushakov.tm.api.service;

import com.ushakov.tm.enumerated.Status;
import com.ushakov.tm.model.AbstractOwnerBusinessEntity;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface IOwnerBusinessService<E extends AbstractOwnerBusinessEntity> extends IOwnerService<E> {

    @NotNull
    E changeStatusById(@Nullable String userId, @Nullable String id, @NotNull Status status);

    @NotNull
    E changeStatusByIndex(@Nullable String userId, @Nullable Integer index, @NotNull Status status);

    @NotNull
    E changeStatusByName(@Nullable String userId, @Nullable String name, @NotNull Status status);

    @NotNull
    E completeById(@Nullable String userId, @Nullable String id);

    @NotNull
    E completeByIndex(@Nullable String userId, @Nullable Integer index);

    @NotNull
    E completeByName(@Nullable String userId, @Nullable String name);

    @NotNull
    E findOneByName(@Nullable String userId, @Nullable String name);

    @Nullable
    E removeOneByName(@Nullable String userId, @Nullable String name);

    @NotNull
    E startById(@Nullable String userId, @Nullable String id);

    @NotNull
    E startByIndex(@Nullable String userId, @Nullable Integer index);

    @NotNull
    E startByName(@Nullable String userId, @Nullable String name);

    @NotNull
    E updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    @NotNull
    E updateByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, @Nullable String description);

}