package com.ushakov.tm.api.endpoint;

import com.ushakov.tm.model.Session;
import org.jetbrains.annotations.NotNull;

import javax.jws.WebMethod;
import javax.jws.WebParam;

public interface ISessionEndpoint {

    @WebMethod
    void closeSession(
            @WebParam(name = "session", partName = "session") @NotNull Session session
    );

    @WebMethod
    @NotNull
    Session openSession(
            @NotNull @WebParam(name = "login", partName = "login") String userLogin,
            @NotNull @WebParam(name = "password", partName = "password") String password
    );

}
