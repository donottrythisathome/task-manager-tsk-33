package com.ushakov.tm.command.project;

import com.ushakov.tm.command.AbstractProjectCommand;
import com.ushakov.tm.endpoint.Project;
import com.ushakov.tm.endpoint.Session;
import com.ushakov.tm.enumerated.Role;
import com.ushakov.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class ProjectFindByNameCommand extends AbstractProjectCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @Nullable
    public String description() {
        return "Find project by name.";
    }

    @Override
    public void execute() {
        @NotNull final Session session = endpointLocator.getSession();
        System.out.println("ENTER PROJECT NAME");
        @NotNull final String projectName = TerminalUtil.nextLine();
        @NotNull final Project project = endpointLocator.getProjectEndpoint().findProjectByName(session, projectName);
        System.out.println(project);
    }

    @Override
    @NotNull
    public String name() {
        return "find-project-by-name";
    }

    @Override
    @Nullable
    public Role[] roles() {
        return Role.values();
    }

}
