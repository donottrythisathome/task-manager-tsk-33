package com.ushakov.tm.service;

import com.ushakov.tm.api.repository.IProjectRepository;
import com.ushakov.tm.api.repository.ITaskRepository;
import com.ushakov.tm.api.service.IProjectTaskService;
import com.ushakov.tm.exception.empty.EmptyIdException;
import com.ushakov.tm.exception.empty.EmptyUserIdException;
import com.ushakov.tm.exception.entity.ProjectNotFoundException;
import com.ushakov.tm.exception.entity.TaskNotFoundException;
import com.ushakov.tm.model.Project;
import com.ushakov.tm.model.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Optional;

public class ProjectTaskService implements IProjectTaskService {

    @NotNull
    private final ITaskRepository taskRepository;

    @NotNull
    private final IProjectRepository projectRepository;

    public ProjectTaskService(@NotNull ITaskRepository taskRepository, @NotNull IProjectRepository projectRepository) {
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
    }

    @Override
    @NotNull
    public Task bindTaskByProjectId(@Nullable final String userId, @Nullable final String projectId, @Nullable final String taskId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        @Nullable final Project project = projectRepository.findOneById(userId, projectId);
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        if (taskId == null || taskId.isEmpty()) throw new EmptyIdException();
        @Nullable final Task task = taskRepository.findOneById(userId, taskId);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        task.setProjectId(projectId);
        return task;
    }

    @Override
    @Nullable
    public Project deleteProjectById(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        @Nullable final List<Task> taskList = taskRepository.findAllByProjectId(userId, projectId);
        if (taskList != null)
            for (@NotNull final Task task : taskList) {
                taskRepository.removeOneById(userId, task.getId());
            }
        return projectRepository.removeOneById(userId, projectId);
    }

    @Override
    @Nullable
    public List<Task> findAllTasksByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        return taskRepository.findAllByProjectId(userId, projectId);
    }

    @Override
    @NotNull
    public Task unbindTaskFromProject(@Nullable final String userId, @Nullable final String taskId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (taskId == null || taskId.isEmpty()) throw new EmptyIdException();
        @Nullable final Task task = taskRepository.findOneById(userId, taskId);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        task.setProjectId(null);
        return task;
    }

}
