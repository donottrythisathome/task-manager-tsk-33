package com.ushakov.tm.api.endpoint;

import com.ushakov.tm.model.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;

public interface IUserEndpoint {

    @WebMethod
    void updateUserPassword(
            @WebParam(name = "session", partName = "session") final Session session,
            @WebParam(name = "password", partName = "password") final String password
    );

}
