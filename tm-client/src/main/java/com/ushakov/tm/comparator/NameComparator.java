package com.ushakov.tm.comparator;

import com.ushakov.tm.api.model.IHasName;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Comparator;

@NoArgsConstructor
public final class NameComparator implements Comparator<IHasName> {

    @NotNull
    private static final NameComparator INSTANCE = new NameComparator();

    @NotNull
    public static NameComparator getInstance() {
        return INSTANCE;
    }

    @Override
    public int compare(@Nullable final IHasName o1, @Nullable final IHasName o2) {
        if (o1 == null || o2 == null) return 0;
        return o1.getName().compareTo(o2.getName());
    }

}
