package com.ushakov.tm.command.project;

import com.ushakov.tm.command.AbstractProjectCommand;
import com.ushakov.tm.endpoint.Session;
import com.ushakov.tm.enumerated.Role;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class ProjectClearCommand extends AbstractProjectCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @Nullable
    public String description() {
        return "Clear project list.";
    }

    @Override
    public void execute() {
        @NotNull final Session session = endpointLocator.getSession();
        System.out.println("[PROJECT CLEAR]");
        endpointLocator.getProjectEndpoint().clearProjects(session);
    }

    @Override
    @NotNull
    public String name() {
        return "project-clear";
    }

    @Override
    @Nullable
    public Role[] roles() {
        return Role.values();
    }

}
