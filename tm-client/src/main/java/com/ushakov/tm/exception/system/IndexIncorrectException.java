package com.ushakov.tm.exception.system;

import com.ushakov.tm.exception.AbstractException;
import org.jetbrains.annotations.NotNull;

public class IndexIncorrectException extends AbstractException {

    public IndexIncorrectException() {
        super("Index is not correct!");
    }

    public IndexIncorrectException(@NotNull String message) {
        super("Entered value " + message + " is not a number!");
    }

}
