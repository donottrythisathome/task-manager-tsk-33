package com.ushakov.tm.command;

import com.ushakov.tm.api.service.EndpointLocator;
import com.ushakov.tm.enumerated.Role;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Setter
@NoArgsConstructor
public abstract class AbstractCommand {

    @Nullable
    protected EndpointLocator endpointLocator;

    @Nullable
    public abstract String arg();

    @Nullable
    public abstract String description();

    public abstract void execute();

    @NotNull
    public abstract String name();

    @Nullable
    public Role[] roles() {
        return null;
    }

    public void setEndpointLocator(@Nullable EndpointLocator endpointLocator) {
        this.endpointLocator = endpointLocator;
    }

}
