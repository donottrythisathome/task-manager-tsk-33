package com.ushakov.tm.exception.empty;

import com.ushakov.tm.exception.AbstractException;

public class EmptyPasswordException extends AbstractException {

    public EmptyPasswordException() {
        super("Error! Password is empty!");
    }

}
