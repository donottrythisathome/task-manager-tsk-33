package com.ushakov.tm.command.project;

import com.ushakov.tm.command.AbstractProjectCommand;
import com.ushakov.tm.endpoint.Project;
import com.ushakov.tm.endpoint.Session;
import com.ushakov.tm.enumerated.Role;
import com.ushakov.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class ProjectCompleteByIdCommand extends AbstractProjectCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @Nullable
    public String description() {
        return "Complete project by id.";
    }

    @Override
    public void execute() {
        @Nullable final Session session = endpointLocator.getSession();
        System.out.println("ENTER PROJECT ID");
        @Nullable final String projectId = TerminalUtil.nextLine();
        @NotNull final Project project = endpointLocator.getProjectEndpoint().completeProjectById(session, projectId);
    }

    @Override
    @NotNull
    public String name() {
        return "complete-project-by-id";
    }

    @Override
    @Nullable
    public Role[] roles() {
        return Role.values();
    }

}
