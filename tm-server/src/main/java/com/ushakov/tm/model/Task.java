package com.ushakov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public final class Task extends AbstractOwnerBusinessEntity {

    @Nullable
    private String projectId;

    public Task(@NotNull String name) {
        this.name = name;
    }

    @Override
    @NotNull
    public String toString() {
        return "ID: " + id + "\nNAME: " + name + "\nDESCRIPTION: " + description
                + "\nSTATUS: " + status.getDisplayName() + "\nPROJECT ID:" + projectId
                + "\nSTART DATE: " + dateStart + "\nCREATED: " + created;
    }

}